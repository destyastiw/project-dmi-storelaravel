@extends('layouts.app')

@section('content')
    <!doctype html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://getbootstrap.com/docs/5.2/assets/css/docs.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"></script>
    </head>

    <body class="p-3 m-0 border-0 bd-example">
        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="img/Hero/Slide1.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="img/Hero/Slide2.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="img/Hero/Slide3.jpg" class="d-block w-100" alt="...">
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
                data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
                data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </body>

    </html>
    {{-- <div class="jumbotron">
        <div class="container">
            <h1 class="display-4">Hello, Guys!</h1>
            <p class="lead">Mari berbelanja di tokovel, disini kami menyediakan semua kebutuh fashion kalian dengan harga
                yang lebih murah</p>
            <hr class="my-4">
            <p>Dapatkan update produk-produk terbaru dari kami hanya di {{ config('app.name') }}</p>
            <a class="btn btn-primary btn-lg" href="#" role="button">Let's Shop</a>
        </div>
    </div> --}}
    <br />
    <div class="container">
        <form action="{{ route('produk.search') }}" method="post">
            @csrf
            <div class="input-group mb-3">
                <input type="text" class="form-control" name="keyword" placeholder="tuliskan nama produk yang dicari">
                <div class="input-group-append">
                    <button type="submit" class="btn btn-primary">Pencarian</button>
                </div>
            </div>
        </form>
    </div>

    <div class="container mb-4">
        <div class="row">
            <div class="col-md">
                <h1>Produk Terbaru</h1>
            </div>
        </div>
        <div class="row">
            @foreach ($produkTerbaru as $prd)
                <div class="col-md-3">
                    <div class="card mt-3">
                        <a href="{{ route('produk.detail', ['slug' => $prd->slug_prd]) }}">
                            <img src="{{ asset('img/produk/' . $prd->gbr_prd) }}" class="card-img-top">
                        </a>
                        <div class="card-body">
                            <h5 class="card-title">{{ $prd->nm_prd }}</h5>
                            <div class="d-flex justify-content-between">
                                <a href="{{ route('keranjang.add', ['produk_id' => $prd->id_prd]) }}"
                                    class="btn btn-sm btn-primary">Masuk Keranjang</a>
                                <small class="text-righ text-primary my-auto">{{ rupiah($prd->hrg_prd) }}</small>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
